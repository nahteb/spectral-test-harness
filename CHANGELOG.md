# Changelog

## `0.3.0` 2022-29-12

Breaking:

- Fixes to make this project compatible with jest 28.0.0 and above only.


## `0.2.0` 2022-01-04

Breaking/changed:

- Remove `resultsForSeverity` from public API
- Return a string `severity`, instead of a numerical one

## `0.1.1` 2021-12-23

- Fixes to make it possible to consume this project correctly

## `0.1.0` 2021-12-23

- Initial release, with key functionality available
